package com.example.jbeck.androidtcpclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
  public static final String EXTRA_MESSAGE = "com.example.jbeck.androidtcpclient.MESSAGE";
  ArrayList<String> q = new ArrayList<String>();
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  public void sendMessage(View view) {
    EditText editText = (EditText)findViewById(R.id.editText);
    String message = editText.getText().toString();
    q.add("C: " + message);
    new AsyncTcpClient().execute(message);
  }

  private class AsyncTcpClient extends AsyncTask<String, Void, String> {
    private static final String SERVER_HOST = "10.0.2.2";
    private static final int SERVER_PORT = 6789;

    protected String doInBackground(String... messages)  {
      String responseFromServer = "";

      try {
        Socket socket = new Socket(SERVER_HOST, SERVER_PORT);
        DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        outToServer.writeBytes(messages[0] + "\n");
        responseFromServer = inFromServer.readLine();
        socket.close();
      }
      catch (Exception e) {
        e.printStackTrace();
      }

      return responseFromServer;

    }

    protected void onPostExecute(String result) {
      setContentView( R.layout.activity_main);
      TextView textView = (TextView)findViewById(R.id.textView2);
      //textView.setText(result);
      q.add("S: " + result);
      printArrayList(q);
    }

    public void printArrayList(ArrayList<String> q) {
      if (q.size() == 11) {
        q.remove(0);
      }
      TextView textView = (TextView) findViewById(R.id.textView2);
      textView.setText(q.get(0));
      for (int i = 1; i < q.size(); ++i) {
        textView.append("\n" + q.get(i));
      }
    }
  }
}